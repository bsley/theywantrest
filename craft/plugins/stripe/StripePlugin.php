<?php 

	namespace Craft;

	class StripePlugin extends BasePlugin
	{
		
		function __construct()
		{
			require CRAFT_PLUGINS_PATH.'/stripe/vendor/autoload.php';
		}

		function getName()
	    {
	         return Craft::t('Stripe Processing');
	    }

	    function getVersion()
	    {
	        return '.1';
	    }

	    function getDeveloper()
	    {
	        return 'Nathanael Moody';
	    }

	    function getDeveloperUrl()
	    {
	        return 'http://nathanaelphilip.com';
	    }

	}	