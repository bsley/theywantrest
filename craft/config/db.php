<?php
/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 * http://buildwithcraft.com/docs/multi-environment-configs
 */

return array(

  '*' => array(
    // The prefix to use when naming tables. This can be no more than 5 characters.
    'tablePrefix' => 'craft',
  ),

  'iwantrest.com' => array(
        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
    'server' => $_ENV['DATABASE_SERVER'],

    // The database username to connect with.
    'user' => 'db176569_phil',

    // The database password to connect with.
    'password' => 'coUv6coaP1Ug1H!',

    // The name of the database to select.
    'database' => 'db176569_rest_production',
    ),

  'theywantrest.dev' => array(
    // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
    'server' => 'localhost',

    // The database username to connect with.
    'user' => 'root',

    // The database password to connect with.
    'password' => 'root',

    // The name of the database to select.
    'database' => 'wewantrest_local',
  ),

  'dev.iwantrest.com' => array(
    // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
    'server' => 'localhost',

    // The database username to connect with.
    'user' => 'nathanae_rest',

    // The database password to connect with.
    'password' => 'IdS7ZSrQg.b]',

    // The name of the database to select.
    'database' => 'nathanae_rest_staging',
  )
);

// local values:

// return array(

//   '*' => array(
//     // The prefix to use when naming tables. This can be no more than 5 characters.
//     'tablePrefix' => 'craft',
//   ),

//   '.dev' => array(
//       // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
//     'server' => 'localhost',

//     // The database username to connect with.
//     'user' => 'root',

//     // The database password to connect with.
//     'password' => 'root',

//     // The name of the database to select.
//     'database' => 'wewantrest_local',
//   )
// );


